#Traduce to 1234
grep -v CHROM experimental_reference_panel_23andme.csv | sed 's/A/1/g' | sed 's/C/2/g' | sed 's/G/3/g' | sed 's/T/4/g' | sed 's/--/NA/g' |cat <(head -1 experimental_reference_panel_23andme.csv) - > experimental_reference_panel_1234.csv
paste -d'\t' <(awk '{print $1"_"$2}' experimental_reference_panel_1234.csv) <(cut -f3- experimental_reference_panel_1234.csv) > experimental_reference_panel_1234_v2.csv
grep -Fwf <(awk '{print $1"_"$2}' 48snps.list) experimental_reference_panel_1234_v2.csv > 48snps_experimental_reference_panel_1234_v2.csv

######################## heatmap of 1234 file ####################################
install.packages("pheatmap")
library(pheatmap)
setwd("~/@R_station/structural_variation/Mauri2Flagiello")
my_matrix= read.csv("experimental_reference_panel_1234_v2.csv", header = T, sep= "\t",row.names = 1)
my_subset=my_matrix[1:100,23:805]
pheatmap(my_subset)
######################## heatmap of 1234 file ####################################
install.packages("pheatmap")
library(pheatmap)
setwd("~/@R_station/structural_variation/Mauri2Flagiello")
my_matrix= as.matrix(read.csv("48snps_experimental_reference_panel_1234_v2.csv", header = T, sep= "\t",row.names = 1))
dim(my_matrix[,23:805])
#[1]  25 783
pheatmap(my_matrix[,23:805],scale = "column", cluster_rows=F,show_rownames =T,clustering_distance_cols = "correlation",cutree_col = 25, fontsize_col = 1)

#partimos de este archivo de 805 clones y 9329 posiciones

#rownames primera columna
sed 's/\t/_/' experimental_reference_panel_23andme_snps_corrected.csv > experimental_reference_panel_23andme_snps_corrected2.csv
#alleles per position
for i in $(seq 2 1 9330); do awk -v var=${i} 'FNR==var{print}' experimental_reference_panel_23andme_snps_corrected2.csv | \
tr -s '\t' '\n' | grep -v 'chr' | awk '!/--/' |sort -V | uniq -c | sort -rnk1 | awk 'FNR<=4{print $2}'| tr -s '\n' '\t' | \
paste - <(echo -e '\t'); done  > alleles_per_position.txt
paste -d'\t' experimental_reference_panel_23andme_snps_corrected2.csv <(cat <(echo -e "1st\t2on\t3th") alleles_per_position.txt) > experimental_reference_panel_23andme_snps_corrected3.csv
#add frequency info
for i in $(seq 2 1 9330); do awk -v var=${i} 'FNR==var{print}' experimental_reference_panel_23andme_snps_corrected2.csv | \
tr -s '\t' '\n' | grep -v 'chr' | awk '!/--/' |sort -V | uniq -c | sort -rnk1 | awk 'FNR<=4{print $1}'| tr -s '\n' '\t' | \
paste - <(echo -e '\t'); done  > number_per_position.txt
paste -d'\t' experimental_reference_panel_23andme_snps_corrected3.csv <(cat <(echo -e "1st\t2on\t3th") number_per_position.txt) > experimental_reference_panel_23andme_snps_corrected4.csv

#transforme in 1,2 or 3 each genotype of each position depending on 1st 2on and 3th more frequent allele.
for i in $(seq 2 806)
do awk -v var=${i} '{if($var==$807) x=1
else if($var==$808) x=2
else if($var==$809) x=3
else x=0; print x}' experimental_reference_panel_23andme_snps_corrected4.csv
done > experimental_reference_panel_23andme_snps_corrected5.csv
for col in $(seq 2 806)
do awk -v var=${col} '{if($var==$807) x=1
else if($var==$808) x=2
else if($var==$809) x=3
else x=0
print x}' experimental_reference_panel_23andme_snps_corrected4.csv > ${col}_experimental_reference_panel_23andme_snps_corrected5.col; done
paste -d'\t' {2..806}_experimental_reference_panel_23andme_snps_corrected5.col | tail -n +2 > experimental_reference_panel_23andme_snps_corrected5.csv
cut -f1 experimental_reference_panel_23andme_snps_corrected4.csv | grep -v CHROM > experimental_reference_panel_23andme_snps_corrected6.csv
paste -d'\t' experimental_reference_panel_23andme_snps_corrected6.csv experimental_reference_panel_23andme_snps_corrected5.csv > experimental_reference_panel_23andme_snps_corrected7.csv
cat <(head -1 experimental_reference_panel_23andme_snps_corrected4.csv |cut -f-806) experimental_reference_panel_23andme_snps_corrected7.csv > experimental_reference_panel_23andme_snps_corrected8.csv #with samples ID
paste -d'\t' SampleID.list AccesionName.list | while read n k; do sed 's/${n}/${k}/g' experimental_reference_panel_23andme_snps_corrected8.csv; done > experimental_reference_panel_23andme_snps_corrected10.csv #with Accesion name


for i in $(seq 2 23); do awk -v var=${i} '$var!=0' experimental_reference_panel_23andme_snps_corrected8.csv | cut -f1,${i},24- > ${i}_vs_modern.csv; done
paste -d'\t' SampleID.list AccesionName.list | while read n k; do sed -i "s/$n/$k/g" *modern.csv; done
for i in $(seq 2 23); do echo ${i}; done > numbers.list
head -1 experimental_reference_panel_23andme_snps_corrected8.csv | cut -f2-23 | tr -s '\t' '\n' > samples.list
paste numbers.list samples.list | while read n k; do mv ${n}_vs_modern.csv ${k}_vs_modern.csv; done

