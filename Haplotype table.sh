#!/bin/bash
# sysinfo_page - A script to genotype different positions of a cohort of samples.

VCF=$1
#VCF is a .vcf file recompliling the calling info of several samples of a cohort, product of the step 9 of GATK_like_calling_adna.sh script. 
LIST=$2
#LIST is a .tsv with at least wo columns: CHROM/ POS. 
MINDP=$3
#MINDP is the min number of alignments per sample over an specific positions. 

echo 'USAGE: ./genotype_me.sh\  <VCF> <LIST> <MINDP>'
echo "I want to genotype"
wc -l $LIST |awk '{print $1}' 
echo "positions" 
echo "in my cohort of samples:" 
grep 'CHROM' $VCF | cut -f10- 
echo "with a min total depth of" $MINDP.  

#10.filtering known positions
grep -Fwf <(awk '{print $1"\t"$2}' $LIST) $VCF > tmp1

#11.genotype positions
for col in 10 11 12 13 14
do
awk -v mindp=$MINDP -v col=${col} 'length($4)==1 && length($5)==1 && $5!~/,/{split ($col, sample,":|,")
if (sample[1]=="./.") geno="NA:.."
else if (sample[1]!="./." && $5!="." && sample[3]==0 && sample[2]>=mindp) geno="REF:"$4$4
else if (sample[1]!="./." && $5!="." && sample[2]==0 && sample[3]>=mindp) geno="HOM:"$5$5
else if (sample[1]!="./." && $5!="." && sample[2]!=0 && sample[3]!=0 && sample[2]+sample[3]>=mindp) geno="HET:"$4$5
else if (sample[1]!="./." && $5=="." && sample[2]>=mindp) geno="REF:"$4$4
else geno="NA:.."
print geno}' tmp1 > ${col}.tmp2 #two columns
cut -d':' -f1 ${col}.tmp2 | sort | uniq -c | awk '{print $2"\t"$1}' > ${col}.tmp3
done

awk -v mindp=$MINDP -v list=$LIST 'length($4)==1 && length($5)==1 && $5!~/,/{print $1"\t"$2"\t"list"\t"$4"\t"$5"\t"$6"\tBQmin5_MQmin20_CONFmin10_DPmin"mindp"\t"$8"\t3geno:2N"}' tmp1 > tmp4
paste -d'\t' 1*.tmp2 > tmp5
echo -e "CHROM\tPOS\tLIST\tREF\tALT\tQUAL\tFILTER\tINFO\tCODE\t56\t57\t58\t86\t91" > header
cat header <(paste -d'\t' tmp4 tmp5) > ${MINDP}_${LIST}_$VCF.csv
paste -d'\t' 1*.tmp3 > ${MINDP}_${LIST}_$VCF.uniq

rm *.tmp*
rm header
