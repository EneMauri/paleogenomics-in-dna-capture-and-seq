################################## 1. genome prep ##############################
cd ../../vitis_info/
download genomes from NCBI 
awk 'BEGIN { ORS = " " } { print }' *plast.fa | sed 's/ //g' | sed 's/>chr20//' | fold -w60 | cat <(echo '>chrCT') - > chrCT.fa
awk 'BEGIN { ORS = " " } { print }' *drion.fa | sed 's/ //g' | sed 's/>chr21//' | fold -w60 | cat <(echo '>chrMT') - > chrMT.fa
cat chr.fa chrCT.fa chrMT.fa > Vitis_vinifera_nuclear_ct_mt.fa
sed 's/TTTAGTTA>chrMT/TTTAGTTA\n>chrMT/' Vitis_vinifera_nuclear_ct_mt.fa > Vitis_vinifera_nc_ct_mt.fa
rm Vitis_vinifera_nuclear_ct_mt.fa
bwa index Vitis_vinifera_nc_ct_mt.fa
######################################################################
