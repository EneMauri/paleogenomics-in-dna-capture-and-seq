for i in $(cat samples.list) 
do 

#1. Create an empty matrix of reads with the following necessary structure:
java -jar /home/icvv/nmauri/tools/picard.jar FastqToSam \
FASTQ=/home/icvv/nmauri/fastq_center/dnaseq/${i}_read1.fastq.gz.gz \
FASTQ2=/home/icvv/nmauri/fastq_center/dnaseq/${i}_read2.fastq.gz.gz \
OUTPUT=1.fasttosam/${i}_fasttosam.bam READ_GROUP_NAME=alldnaseq \
SAMPLE_NAME=${i} PLATFORM_UNIT=CA3CMANXX PLATFORM=illumina

#2. Mark Illumina adapter sequences
java -jar /home/icvv/nmauri/tools/picard.jar MarkIlluminaAdapters \
I=1.fasttosam/${i}_fasttosam.bam \
O=2.markillumina/${i}_markillumina.bam M=2.markillumina/${i}_markillumina.txt

#3. Mask Illumina adapters
java -jar /home/icvv/nmauri/tools/picard.jar SamToFastq \
I=2.markillumina/${i}_markillumina.bam FASTQ=3.samtofastq/${i}_samtofastq.fq \
CLIPPING_ATTRIBUTE=XT CLIPPING_ACTION=2 INTERLEAVE=true NON_PF=true

#4. bwa-mem alignment (-k15) 
#-k is the minimum seed length or number of read's bases that match exactly with the reference. 
# This is a requirement for the aligner to start the alignment.
# My previous alignment performance using this parameter indicated this is the optimize value. 
bwa mem -k15 -M -p /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
3.samtofastq/${i}_samtofastq.fq | samtools view -Sb - > 4.alignments/${i}_bwamem15.bam

#5. Merge single and paired data
java -jar /home/icvv/nmauri/tools/picard.jar MergeBamAlignment \
R=/home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
ALIGNED_BAM=4.alignments/${i}_bwamem15.bam \
UNMAPPED_BAM=2.markillumina/${i}_markillumina.bam O=5.merge/${i}_merge.bam \
CREATE_INDEX=true ADD_MATE_CIGAR=true CLIP_ADAPTERS=false \
CLIP_OVERLAPPING_READS=true INCLUDE_SECONDARY_ALIGNMENTS=true \
MAX_INSERTIONS_OR_DELETIONS=-1 \
PRIMARY_ALIGNMENT_STRATEGY=MostDistant ATTRIBUTES_TO_RETAIN=XS

#6. Mark duplicates
java -jar /home/icvv/nmauri/tools/picard.jar MarkDuplicates I=5.merge/${i}_merge.bam O=6.markdup/${i}_markdup.bam \
M=6.markdup/${i}_markdup.txt OPTICAL_DUPLICATE_PIXEL_DISTANCE= 2500 CREATE_INDEX=true

#7. Indel realignment
java -jar /home/icvv/nmauri/tools/GenomeAnalysisTK.jar -T RealignerTargetCreator \
-R /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
-I 5.merge/${i}_merge.bam -o 7.indel_realign/${i}_forIndelRealigner.intervals
java -jar /home/icvv/nmauri/tools/GenomeAnalysisTK.jar -T IndelRealigner \
-R /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
-I 6.markdup/${i}_markdup.bam \
-targetIntervals 7.indel_realign/${i}_forIndelRealigner.intervals \
-o 7.indel_realign/${i}_realigned.bam

#8. All-positions calling with Haplotypecaller tool and emit reference confidence mode.
#This mode ERC also do the haplotype phasing, what is to say, the positional relationship between the SNPS in the same chromosome.  
#ERC = GVCF output records for all siyes in gVCF format, by blocks of non-variant data.
#ERC = BP_RESOLUTION  output records for all siyes in VCF format, one line per position.
#Haplotype caller default conditions:
#num_cpu_threads_per_data_thread=1
#heterozygosity=0.001 indel_heterozygosity=1.25E-4
#min_base_quality_score=10
#min_mapping_quality_score=20
#max_alternate_alleles=6 input_prior=[] sample_ploidy=2 genotyping_mode=DISCOVERY
#kmerSize=[10, 25]

java -jar /home/icvv/nmauri/tools/GenomeAnalysisTK.jar -T HaplotypeCaller \
-R /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
--emitRefConfidence BP_RESOLUTION -variant_index_type LINEAR \ 
-variant_index_parameter 128000 -I 7.indel_realign/${i}_realigned.bam \
-o 9.erc_bp/${i}_raw.snps.indels.g.vcf
#standard_min_confidence_threshold_for_calling=-0.0 
#standard_min_confidence_threshold_for_emitting=-0.0

#SNP filtering.
#9.Joint genotyping on all samples together 
java -jar /home/icvv/nmauri/tools/GenomeAnalysisTK.jar -T GenotypeGVCFs \
-R /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
-V  ...
-V  ...
-V  ...
-o 9.genotyping/output.vcf

#Alternative haplotype caller with lower quality filtering
java -jar /home/icvv/nmauri/tools/GenomeAnalysisTK.jar -T HaplotypeCaller \
-R /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
--emitRefConfidence BP_RESOLUTION -variant_index_type LINEAR -variant_index_parameter 128000 \
--min_base_quality_score 5 --min_mapping_quality_score 1 \
-I 7.indel_realign/${i}_realigned.bam -o 9.erc_bp/${i}_raw.snps.indels.Q1q5.g.vcf

#10.Matrix convertion to hom,het,and,ref.
./plink2 --vcf 10.genotyping/ancientdna.vcf --snps-only --allow-extra-chr --recode A-transpose --threads 8 --out plink_Atranspose.txt 
done
#https://www.cog-genomics.org/plink/2.0/formats
#.traw (variant-major additive component file)
