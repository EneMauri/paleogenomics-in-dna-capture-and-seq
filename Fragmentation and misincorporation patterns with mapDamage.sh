# Compare fragmentation and misincorporation patterns for the ancient and modern samples
for i in $(ls ../2.bam_files/*realigned.bam)
do 
mapDamage -i ${i} -r /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa -v --no-stats
done
