###>>>> INSTRUCTIONS 
###>>>> https://bioinformaticsworkbook.org/dataAcquisition/fileTransfer/sra.html
###>>>> https://www.ncbi.nlm.nih.gov/books/NBK179288/

#buscar archivos de secuenciación en 
https://www.ncbi.nlm.nih.gov/sra
#escribiendo el taxon id:
Para vid hay 3 taxon ids publicados
vinifera: txid29760
sylvestris : txid755351

NCBI TAXONOMY
https://www.ncbi.nlm.nih.gov/taxonomy/Vitis[All Names] 

#búsqueda avanzada
#Vitis vinifera ssp vinifera
txid29760 AND ("biomol dna"[Properties] AND "strategy wgs"[Properties] AND "library layout paired"[Properties] AND "platform illumina"[Properties]
#Vitis vinifera ssp sylvestris 
txid755351 AND ("biomol dna"[Properties] AND "strategy wgs"[Properties] AND "library layout paired"[Properties] AND "platform illumina"[Properties]

DNA-Seq of Vitis vinifera subsp sylvestris : individual Dirmstein-mle
/home/icvv/nmauri/tools/sratoolkit.2.9.6-1-centos_linux64/bin/fastq-dump --split-files --origfmt --gzip SRR8835138

######################### PROYECTO DOMESTICACION
#13 individuals from 11 vinifera cultivars and nine sylvestris and one V.rotundifoia
Evolutionary genomics of grape (Vitis vinifera ssp. vinifera) domestication
https://www.pnas.org/content/pnas/suppl/2017/10/11/1709257114.DCSupplemental/pnas.1709257114.sapp.pdf
The raw sequencing data have been 
deposited in the Short Read Archive at NCBI under
BioProject ID: PRJNA388292

PROJECT=$1
Downloading all SRA files related to a BioProject/study
#To get the SRR numbers associated with the project:
#esearch installation
#copy and chmod 733 this script
https://www.ncbi.nlm.nih.gov/books/NBK179288/
source ./install-edirect.sh
esearch -db sra -query SRP009057 | efetch --format runinfo | sort -t',' -rVk11 > SRR_samples.csv
awk -F',' '/^SRR/{print $1}' SRR_samples.csv > SRR.numbers

#Make sure you do this on Condoddtn node or as a PBS job
#To download them all in parallel (limit the number to 3 concurrent downloads)
#parallel --jobs 3 "fastq-dump --split-files --origfmt --gzip {}" ::: SRR.numbers no funciona!
for i in $(cat SRR.numbers); do fastq-dump --split-files --origfmt --gzip ${i}; done

######################### PROYECTO SULTANINA CHILE
https://bmcplantbiol.biomedcentral.com/articles/10.1186/1471-2229-14-7#additional-information
https://www.ncbi.nlm.nih.gov/bioproject/PRJNA207665

######################### PROYECTO VARIACION ESTRUCTURAL ITALIA
https://trace.ncbi.nlm.nih.gov/Traces/sra/?study=SRP009057

