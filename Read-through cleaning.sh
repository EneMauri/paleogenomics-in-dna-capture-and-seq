#!/bin/bash
# Read-through means that the sample DNA fragment being sequenced is shorter than the read length, such that the 3' end of one read includes the reverse-complement of the adapter from the start of the other read.
# Read-through in ancient DNA sequencing is a common event due to the degradation state of these samples.  
######################## 1. Remove 3' end adapter sequences.

ls *read1.fastq.gz | awk -F'_' '{print $1"_"$2"_"$3}' > samples.list
mkdir cutadapt-adapter && cd cutadapt-adapter

#only reads without the adapter (125 bp)
for i in $(cat ../samples.list) 
do
cutadapt -a AGATCGGAAGAGC -A AGATCGGAAGAGC \
--discard-trimmed \
-o ${i}_read1_noadapter.fastq.gz -p ${i}_read2_noadapter.fastq.gz \
../${i}_read1.fastq.gz ../${i}_read2.fastq.gz
done >> cutadapt_noadapter.report

#only reads with the Illumina adapter (min 13nt)
for i in $(cat ../samples.list)
do 
cutadapt -a AGATCGGAAGAGC -A AGATCGGAAGAGC -m25 -M112 \
--discard-untrimmed \
-o ${i}_read1_adapter.fastq.gz -p ${i}_read2_adapter.fastq.gz \
../${i}_read1.fastq.gz ../${i}_read2.fastq.gz
done  >> cutadapt_adapter.report

#table
cat ../samples.list | awk -F'_' '{print $1}' > rownames
grep 'Total read pairs processed' cutadapt_adapter.report | awk -F':' '{print $2}' | sed 's/,//g' | sed 's/ //g' > tmp1 ##total of reads
grep 'passing filters' cutadapt_adapter.report | awk -F':| bp' '{print $2}' | sed 's/,//g' | sed 's/ //g' | sed 's/)//g' | sed 's/(/ /g'> tmp2 ##reads with adapter (13 nt at least) 
grep 'passing filters' cutadapt_noadapter.report | awk -F':| bp' '{print $2}' | sed 's/,//g' | sed 's/ //g'| sed 's/)//g' | sed 's/(/ /g' > tmp3 ##reads without adapter
paste -d' ' rownames tmp1 tmp2 tmp3 > tmp4
cat <(echo 'sample total adapter adapter% noadapter noadapter%') tmp4 | sort -n -k1 > table
