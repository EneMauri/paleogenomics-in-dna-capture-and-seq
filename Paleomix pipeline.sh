########################## 1. adapter removal ###############################
https://github.com/MikkelSchubert/adapterremoval

command line based on:

Centre for GeoGenetics, Natural History Museum of Denmark, University of Copenhagen - http://geogenetics.ku.dk/research/research_groups/palaeomix_group/

# Trimming
ls *read1.fastq.gz | awk -F'_' '{print $1"_"$2"_"$3}'> samples.list 
for sample in $(cat samples.list)
do
AdapterRemoval \
--file1 <(gzip -cd ${sample}_read1.fastq.gz) \
--file2 <(gzip -cd ${sample}_read2.fastq.gz) \
--trimns \
--trimqualities \
--minlength 20 \ # min length of reads 20
--qualitybase 33  \
--collapse \ # INSERTS with min length 29  (20 b + 20 b -11 bp = 29 insert distance)  
--mm 3 \ max mismatches 3
--settings ${sample}.settings \
--discarded >(gzip > ${sample}.discard.fastq.gz) \  #<20 bp collapsed PE
--outputcollapsed >(gzip > ${sample}.collapsed.fastq.gz) \  #29-239 bp collapsed PE
--outputcollapsedtruncated >(gzip > ${sample}.collapsed.trunc.fastq.gz) \
--singleton >(gzip > ${sample}.singleton.trunc.fastq.gz) \
--output1 >(gzip > ${sample}.pair1.fastq.gz) \  #125 nt read1
--output2 >(gzip > ${sample}.pair2.fastq.gz) #125nt read2
done 


# Alignment
for i in $(ls*.collapsed.fastq.gz)
do 
bwa aln -l 1024 /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa ${i} | \
bwa samse /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa - ${i} | \
samtools view -bSh -q 30 -F0x4 - | samtools sort - -o ${i}.sort ; done

# Duplicates removal 
for i in $(ls *collapsed.fastq.gz.sort.bam)
do 
java -jar /home/icvv/nmauri/tools/picard.jar MarkDuplicates I=${i} \
O=${i}.markdup.bam AS=TRUE M=${i}.markdup.txt REMOVE_DUPLICATES=TRUE #INDEX=TRUE
done
#depth

# Rough estimate of coverage: number of bases of aligned reads divided by length of the reference
for i in $(ls *markdup.bam)
do 
paste -d'\t' <(echo ${i}) <(samtools view ${i} | \
awk '{sum+=length($10)}END{print sum" bases, "NR" reads"}')
done > bases_reads.txt

56_2.collapsed.fastq.gz.sort.bam.markdup.bam	4046185 bases, 65846 reads
56_3.collapsed.fastq.gz.sort.bam.markdup.bam	52574 bases, 771 reads
57_2.collapsed.fastq.gz.sort.bam.markdup.bam	97624198 bases, 1171796 reads
57_3.collapsed.fastq.gz.sort.bam.markdup.bam	4462768 bases, 45146 reads
58_2.collapsed.fastq.gz.sort.bam.markdup.bam	2122050 bases, 26839 reads
58_3.collapsed.fastq.gz.sort.bam.markdup.bam	206798 bases, 2251 reads
86_2.collapsed.fastq.gz.sort.bam.markdup.bam	224590 bases, 3578 reads
86_3.collapsed.fastq.gz.sort.bam.markdup.bam	37311 bases, 571 reads
91_2.collapsed.fastq.gz.sort.bam.markdup.bam	1163940 bases, 15357 reads
91_3.collapsed.fastq.gz.sort.bam.markdup.bam	46084 bases, 620 reads

# Compare fragmentation and misincorporation patterns for the ancient and modern samples
for i in $(ls *markdup.bam)
do mapDamage -i ${i} -r /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa -v --no-stats
done

# Calling
ls *2.collapsed.fastq.gz.sort.bam.markdup.bam > non_enri.list
ls *3.collapsed.fastq.gz.sort.bam.markdup.bam > enri.list

bcftools mpileup -f /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
-b non_enri.list --skip-indels -R 12000snps.list --annotate AD > my_non_enrichment.vcf
bcftools mpileup -f /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
-b enri.list --skip-indels -R 12000snps.list --annotate AD > my_enrichment.vcf

