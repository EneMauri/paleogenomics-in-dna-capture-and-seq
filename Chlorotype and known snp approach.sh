for i in $(cat ../samples.list); do awk '$10!="0/0:0,0:0:0:0,0,0"' ${i}_raw.snps.indels.g.vcf > filter0/${i}_raw.snps.indels_filter0.g.vcf; done
for i in $(cat ../../samples.list); do awk '/^#/ || $1=="chrCT"' ${i}_*raw.snps.indels_filter0.g.vcf > chrCT/${i}_*raw.snps.indels_filter0_chrCT.g.vcf ; done
"SNP_name 
Javier Ibañez"	SNP Target	Position	Allele Chlorotype A	Allele Chlorotype B	Allele Chlorotype C	Allele Chlorotype D	Allele Chlorotype NV	DifferentialChlorotype (unknown in B)	Allele Diff Chlorotype
SNP_NG_C_003	[C/T]	7065	C	T	T	C	C	Chlorotype C+B+B1	T
No presente en chip	[A/G]	14416	G/A	G	G	G	G	Chlorotype A	A
No presente en chip	[A/C]	24437	C	A	A	A	A	Non-A	A
SNP_NG_AS_001	[A/C]	33406	A	C	A	A	A	Clorotypes AE1+R+B+RU5 (no B1)	C
SNP_NG_D_003	[A/G]	73765	G	G	G	A	G	Chlorotype D	A
SNP_NG_C_001	[C/T]	75398	C	C	T	C	C	Chlorotype C	T
No presente en chip	[C/T]	77099	C	C	T	T	C	Chlorotype C+D	T
SNP_NG_NV_2	[C/T]	117253	C	C	C	C	T	Non-vinifera	T
No presente en chip	[G/T]	119571	T	T	G	G	T	Non-(C+D)	G
SNP_NG_D_001	[C/T]	128420	C	C	C	T	C	Chlorotype D	T
No presente en chip	[A/C]	133982	A	C	C	A	C	Chlorotype A+D	C

for i in $(ls *indels_filter0_chrCT.g.vcf); do for snp in 7065 14416 24437 33406 73765 75398 77099 117253 119571 128420 133982 ; do grep -w ${snp} ${i} > ${i}.11snps; done; done
for i in $(ls *chrCT.Q1q5.g.vcf); do echo ${i} && grep -Fwf <(awk '{print $1"\t"$2}' ../../../chlorotypeAD.list) ${i}| awk '{print $10}' | awk -F':' '{print $2}' | awk -F ',' '{print $1}' | awk 'BEGIN{ORS="\t"}{print}' ; done



