###Take a look to the reads sequences in order to measure the quality and the presence of the expected ancient dna.

###convert fastq reads to fasta sequences 
#(at cutadapt_adapter folder)
for i in $(ls *_read*_*adapter.fastq.gz)
#take out the read name and the sequence 
do zcat ${i} | grep -A1 -B0 @D00733 | \
sed 's/--//g' | sed 's/@D00733/>@D00733/' | \
#remove empty lines
sed '/^$/d' > ${i}.fa
done

###compare read sequences with databases using megablast algorithm. 
#Megablast is the algorith recommended for sequence identification but depending on the results, we will see if this can be applied to ancient dna.
#If differences due to particular features of ancient dna sequencing (gaps, errors), or unexpected taxonomic differences with reference, we will applied blastn.

for i in $(ls *read1_adapter.fastq.gz.fa)
do 
./../../tools/ncbi-blast-2.7.1+/bin/blastn -task megablast -num_threads 8 \
-query ${i} -db ../../tools/ncbi-blast-2.7.1+/bin/nt_db/nt \
-outfmt '6 qseqid sseqid qstart qend length nident mismatch gapopen pident evalue staxids stitle' \
-num_alignments 1 -window_size 0 -out ${i}.megablast
done

#"-num_alignments 1" limits the number of hits to 1 database sequence.
#"-window_size 0" disables multiple hits for differents segments of the query sequence.  
#"-word_size" is 28 base pairs by default
# you could use the same command line with the option "-remote" to run it in ncbi servers

