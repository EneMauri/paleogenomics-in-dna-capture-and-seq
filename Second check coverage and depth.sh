######################## get mapq20 cov and aln stats ##########################
mkdir 3.mpileup_files
for i in $(ls *filtered.bam) 
do 
# total number of bases at ref genome: 486198630 bp
# total number of Ns at ref genome: 15991470 bp
# So, => 470207160
samtools mpileup -q20 -f ../../vitis_info/chr.fa ${i} | \
awk -v var=${i} '$3!="N" && $4>0 {sum+=$4; sumsq+=$4*$4}END{print var, NR/470207160, sum/NR, sqrt(sumsq/NR - (sum/NR)**2)}' \
> 3.mpileup_files/${i}.cov ## results for coverage, depth of coverage, and, standart deviation of depth.
cat <(echo sample cov depth_avr SD_depth_avr) *.cov > coverage.csv

samtools view -bh -q20 ${i} | samtools flagstat - > 3.mpileup_files/${i}.flags
done
ls  *.flags | awk -F'_' '{print $1}' > samples.list
for i in $(cat samples.list); do awk '/read1/{print $1}' ${i}*.flags; done
for i in $(cat samples.list); do awk '/read2/{print $1}' ${i}*.flags; done
for i in $(cat samples.list); do awk '/properly paired/{print $1}' ${i}*.flags; done
for i in $(cat samples.list); do awk '/singletons/{print $1}' ${i}*.flags; done
for i in $(cat samples.list); do awk '/mapQ>=5/{print $1}' ${i}*.flags; done
cat <(echo "sample,read1,read2,properly_paired,singletons,diff_chr") <(paste -d',' samples.list read1 read2 properly_paired singletons diff_chr) > mapq20_report.csv
cat

############################################## second part #####################################
now we want to know whether these sequences are only targetting better vitis vinifera than other organisms. 
previous analysis:
awk '$10~/N/' 9_27432_AACGACGT-GGAGAACG_bwa2580_sorted_rmdup_filtered.sam| wc -l
561  #some Ns not so frequent 
awk '{print length($10)}' 9_27432_AACGACGT-GGAGAACG_bwa2580_sorted_rmdup_filtered.sam| sort -V | uniq -c | awk '{print $2,$1}' 
###size peak between 36 and 42 
awk '{print $3}' 9_27432_AACGACGT-GGAGAACG_bwa2580_sorted_rmdup_filtered.sam| sort -V | uniq -c
###chromosome enrichment chrCT chrMT and random chr5 in this order and taking into account the size of the chromosome. 
awk '/XC:i:/{print $13}' 9_27432_AACGACGT-GGAGAACG_bwa2580_sorted_rmdup_filtered.sam| awk -F':' '{print $3}' | sort -V | uniq -c
###some N and some insetions and deletion that even the event, leave a good matching length
samtools view -H 9_27432_AACGACGT-GGAGAACG_bwa2580*.bam > header.txt
cat header.txt 9_27432_AACGACGT-GGAGAACG_bwa2580_sorted_rmdup_filtered.sam | samtools view -hSb - > 9_27432_AACGACGT-GGAGAACG_bwa2580_sorted_rmdup_filtered.bam   
samtools depth 9_27432_AACGACGT-GGAGAACG_bwa2580_sorted_rmdup_filtered.bam | awk '{print $3}' | sort -V | uniq -c 
   1142 0
 147096 1
 415345 2
   1408 3
   2135 4
     31 5
    190 6
      7 7
    118 8
      4 9
    101 10
     17 11
     24 12
     10 13
      1 15
     28 16
      7 17
     24 18
      2 19
      5 20
###looks some inserts have more depth, which are the good ones, the more abundant or the less?

###em serie:

for i in $(cat ../samples.list); do cat <(samtools view -H ${i}_bwa2580_sorted_rmdup.bam) ${i}_bwa2580_sorted_rmdup_filtered.sam | samtools view -hSb - > ${i}_bwa2580_sorted_rmdup_filtered.bam; done
[icvv@hidalgo cutadapt]$ for i in $(cat ../samples.list); do samtools depth ${i}_bwa2580_sorted_rmdup_filtered.bam | awk '$3>=6' | awk '{print $1}' | sort -V | uniq -c | awk '{print $2,$1}' && echo ${i}; done
102_27448_CCAGCAAT-CGCTGCTG
10_27433_AATCCAGT-ATCGCGAG
103_27449_CTGATACG-AATCGCTG
104_27450_GAGGCTTA-ATCGCGAG
106_27451_GGTCTGAG-GTACGTCC
107_27452_GGTAGGCA-TAGTAGCC
chr2 13
chr17 61
chrUn 251
18_27435_CTGATACG-AACTGGTT
37_27436_GTATCTCA-CTGATCGA
38_27437_TACGCATC-GTAACCAA
4_27434_CCGCGTAA-CCTCCGTT
44_27438_ATCAACCA-CCTCCGTT
45_27439_CGTCATAC-TCTCTCGT
56_27440_AGGTCCTT-CGCTGCTG
chr2 88
chr7 74
chr15 5
chr17 77
chrUn 496
57_27441_AATCCAGT-GGAGAACG
58_27442_TACGCATC-ATCGCGAG
61_27443_GCAGGTTG-AGTATGCC
chr17 37
chrUn 49
81_27444_CTGATACG-GTCTAACC
chr17 31
82_27445_TCTAGAGT-TAGGTTGA
86_27446_ACCGATTC-CTTGCCGT
91_27447_AGATGAGT-GCGCCAGT
chr12 43
chr17 116
chrUn 379
9_27432_AACGACGT-GGAGAACG
######################################## so, samples 18, 57, 81, 82, and 9 are good looking !!!! and chr17 may be consider as a marker of ancient dna.
chr17 is the smallest chromosome i Vitis which is in accourdance with the chromosomal domain theory! that says the smallest chromosomes are positioned in the center of the cell-
###
This may mean at the same time than the rate of degradation in the nuclei dna positioned in the center is lower than a bunch of chloroplasts and mitochondrial per cell. 
###
for i in $(cat ../samples.list); do samtools depth ${i}_bwa2580_sorted_rmdup_filtered.bam | awk '$3>=6' | awk '/chr17/' && echo ${i}; done 
###muy fuerte!!! the segment from 10422809 to 10422999 seems to be well represented (equal or grater than 6) in all the nice samples, casi 200 nt en el centro. 
##mpieup where each line consists of chromosome, 1-based coordinate, reference base, the number of reads covering the site, read bases and base qualities. At the read base column, a dot stands for a match to the reference base on the forward strand, a comma for a match on the reverse strand.
samtools mpileup -O -f ../../vitis_info/Vitis_vinifera_nc_ct_mt.fa -r chr17:10422809-10422999 9_27432_AACGACGT-GGAGAACG_bwa2580_sorted_rmdup_filtered.bam
samtools mpileup -r chr17:10422809-10422999 -f ../../vitis_info/Vitis_vinifera_nc_ct_mt.fa -b 5samples_filtered_bam.list 
###convert the bam in a fasta format
for i in $(cat ../samples.list); do samtools bam2fq ${i}_bwa2580_sorted_rmdup_filtered.bam | grep -A1 -B0 @D00733 | sed 's/--//g' | sed 's/@D00733/>@D00733/g' > ${i}.fa; done
[icvv@hidalgo cutadapt]$ for i in $(ls *.fa); do grep -c '>@D00733' ${i}; done 
0
0
0
0
8
2
1476
44
59
36
38
248
360
4163
89
6
562
1066
11
60
20249


