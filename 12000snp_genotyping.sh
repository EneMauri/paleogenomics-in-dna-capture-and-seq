[icvv@hidalgo AncientDNA]$ cat 12000snps_vcf2geno.sh

################ calling at all positions ###############################

################ extracting specific places #############################
awk '{print $1"\t"$2-1"\t"$2"\t"$3}' /home/icvv/nmauri/new_ancientDNA/12000snps.list > 12000snps.bed
ls *_realigned.bam | awk -F'_' '{print $1"_"$2"_"$3}' > samples.list
for sample in $(cat samples.list)
do 
java -jar /home/icvv/nmauri/tools/GenomeAnalysisTK.jar -T HaplotypeCaller \
-R /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
-L 12000snps.bed \
--emitRefConfidence BP_RESOLUTION -variant_index_type LINEAR -variant_index_parameter 128000 \
--min_base_quality_score 5 --min_mapping_quality_score 20 --dontUseSoftClippedBases \
-I ${sample}_realigned.bam -o ${sample}_12000snps.vcf 
done

############## cohort of samples ########################################
SAMPLES=$(ls *_12000snps.vcf | sed 's/^/--variant /g')

java -jar /home/icvv/nmauri/tools/GenomeAnalysisTK.jar \
-T GenotypeGVCFs --includeNonVariantSites \
-R /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
-o 12000snps_cohort.vcf \
$(echo $SAMPLES)

############### 4 fields conversion ####################################### 
COL_NUMBER=$(grep '^#CHROM' 12000snps_cohort.vcf | awk '{print NF}')
grep '^#CHROM' 12000snps_cohort.vcf> header
cut -f-9 12000snps_cohort.vcf | grep -v '^#'> front
for col in $(seq 10 $COL_NUMBER)
do awk -v var=$col '!/#/{split ($var,c,":"); print c[2]}' 12000snps_cohort.vcf | \
awk -F',' '{if (NF==1) print $0",0,0,0"
else if (NF==2) print $0",0,0"
else if (NF==3) print $0",0"
else if (NF==4) print $0
else print "ERROR"}' > ${col}_12000snps.count
done
grep ERROR *12000snps.count
paste -d'\t' front <(paste -d'\t' *_12000snps.count) |cat header - > 12000snps_counting.csv

################# merge pre and postcapture samples ######################
#change file names
paste <(ls *_12000snps.count) <(cut -f10- header | tr -s '\t' '\n') |\
while read n k; do mv ${n} ${k}_12000snps.count
done

#joint counting
for sample in 102 103 10 104 106 18 37 38 45 56 57 58 81 82 86 91 9
do paste -d',' ${sample}_2*_12000snps.count ${sample}_3*_12000snps.count |\
awk -F',' '{print $1+$5","$2+$6","$3+$7","$4+$8}' > ${sample}_merged_12000snps.count
rm ${sample}_2*_12000snps.count && rm ${sample}_3*_12000snps.count
done
cat <(ls *_12000snps.count | awk -F'_' '{print $1}' | tr -s '\n' '\t' | sed 's/$/\n/') <(paste -d'\t' *_12000snps.count) |\
paste -d'\t' <(cut -f-9 12000snps_counting.csv) - > 12000snps_merging.csv 

#define genotype
COL_NUMBER=$(grep '^#CHROM' 12000snps_merging.csv | awk '{print NF}')
grep '^#CHROM' 12000snps_merging.csv > header

for col in $(seq 10 $COL_NUMBER)
do awk -v var=${col} '!/#/{split($var,b,",")
if(b[3]>0||b[4]>0) print "--" #FILTER_CONTAMINATION
else if(b[1]+b[2]==0) print "--" #FILTER_ND
else if(b[1]+b[2]<3) print "--" #FILTER_DP
else if(b[1]+b[2]>=3 && (b[1]+0.0001)/(b[1]+b[2])>=0.90) print $4$4
else if(b[1]+b[2]>=3 && (b[2]+0.0001)/(b[1]+b[2])>=0.90) print $5$5
else if(b[1]+b[2]>=3 && (b[1]+0.0001)/(b[1]+b[2])>0.10 && (b[1]+0.0001)/(b[1]+b[2])<0.90) print $4$5
else print "ERROR"}' 12000snps_merging.csv > ${col}_12000snps_merging.geno
done
paste -d'\t' front <(paste -d'\t' *_12000snps_merging.geno) | \
cat header - > 12000snps_genotyping.csv
