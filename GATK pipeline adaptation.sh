#for sample in 56 57 58 86 91; do ls ${sample}*_read1.fastq.gz ; done
#56_34636_AGGTCCTT-CGCTGCTG_read1.fastq.gz
#57_34637_AATCCAGT-GGAGAACG_read1.fastq.gz
#58_34638_TACGCATC-ATCGCGAG_read1.fastq.gz
#86_34641_ACCGATTC-CTTGCCGT_read1.fastq.gz
#91_34643_AGATGAGT-GCGCCAGT_read1.fastq.gz

########################## starting reads number ##############################

for i in $(ls *_read1.fastq.gz); do zcat ${i} | grep -c '@D00733' ; done

########################## GATK-like calling  ################################# 
for sample in 56_34636_AGGTCCTT-CGCTGCTG 57_34637_AATCCAGT-GGAGAACG 58_34638_TACGCATC-ATCGCGAG 86_34641_ACCGATTC-CTTGCCGT 91_34643_AGATGAGT-GCGCCAGT
do

#1. Create an empty matrix of reads with the following necessary structure:
java -jar /home/icvv/nmauri/tools/picard.jar FastqToSam \
FASTQ=${sample}_read1.fastq.gz \
FASTQ2=${sample}_read2.fastq.gz \
OUTPUT=${sample}_fasttosam.bam READ_GROUP_NAME=alldnaseq \
SAMPLE_NAME=${sample} PLATFORM_UNIT=CA3CMANXX PLATFORM=illumina

#2. Mark illumina adapter sequences
java -jar /home/icvv/nmauri/tools/picard.jar MarkIlluminaAdapters \
I=${sample}_fasttosam.bam \
O=${sample}_markillumina.bam M=${sample}_markillumina.txt


#3.mask illumina adapters
java -jar /home/icvv/nmauri/tools/picard.jar SamToFastq \
I=${sample}_markillumina.bam FASTQ=${sample}_samtofastq.fq \
CLIPPING_ATTRIBUTE=XT CLIPPING_ACTION=2 INTERLEAVE=true NON_PF=true

#4. bwa-mem alignment
bwa mem -k15 -M -p /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
${sample}_samtofastq.fq | samtools view -Sb - > ${sample}_bwamem15.bam

at this point adapters are marked with '#' in the QUAL which means is ASCII characters base quality 2.
So, these bases are not to count for snps analysis.

#5. Merge single and paired data
java -jar /home/icvv/nmauri/tools/picard.jar MergeBamAlignment \
R=/home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
ALIGNED_BAM=${sample}_bwamem15.bam \
UNMAPPED_BAM=${sample}_markillumina.bam O=${sample}_merge.bam \
CREATE_INDEX=true ADD_MATE_CIGAR=true CLIP_ADAPTERS=false \
CLIP_OVERLAPPING_READS=true INCLUDE_SECONDARY_ALIGNMENTS=true \
MAX_INSERTIONS_OR_DELETIONS=-1 \
PRIMARY_ALIGNMENT_STRATEGY=MostDistant ATTRIBUTES_TO_RETAIN=XS

#6.mark duplicates
java -jar /home/icvv/nmauri/tools/picard.jar MarkDuplicates I=${sample}_merge.bam O=${sample}_markdup.bam \
 M=${sample}_markdup.txt OPTICAL_DUPLICATE_PIXEL_DISTANCE= 2500 CREATE_INDEX=true

#7.indel_realignment
java -jar /home/icvv/nmauri/tools/GenomeAnalysisTK.jar -T RealignerTargetCreator \
-R /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
-I ${sample}_merge.bam -o ${sample}_forIndelRealigner.intervals

java -jar /home/icvv/nmauri/tools/GenomeAnalysisTK.jar -T IndelRealigner \
-R /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa\
-targetIntervals ${sample}_forIndelRealigner.intervals \
-I ${sample}_markdup.bam \
-o ${sample}_realigned.bam

########## mapq low quartile study in order to decide the mapq value to call for
for i in $(ls *realigned.bam)
do 
samtools view -F12 -q1 -F256 -F1024 ${i} | cut -f5 > ${i}.mapq
./quartile.R ${i}.mapq
done

#8. calling 
java -jar /home/icvv/nmauri/tools/GenomeAnalysisTK.jar -T HaplotypeCaller \
-R /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
--emitRefConfidence BP_RESOLUTION -variant_index_type LINEAR -variant_index_parameter 128000 \
--min_base_quality_score 5 --min_mapping_quality_score 20 --dontUseSoftClippedBases \
-I ${sample}_realigned.bam -o ${sample}_raw.snps.indels.vcf -bamout ${sample}_raw.snps.indels.bam
done

#alter 8
java -jar /home/icvv/nmauri/tools/GenomeAnalysisTK.jar -T HaplotypeCaller \
-R /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
-L 12000snps.bed
--emitRefConfidence BP_RESOLUTION -variant_index_type LINEAR -variant_index_parameter 128000 \
--min_base_quality_score 5 --min_mapping_quality_score 20 --dontUseSoftClippedBases \
-I ${sample}_realigned.bam -o ${sample}_htcaller_5_20.vcf 
done

#convert to 4 digits every positions
for i in $(ls *htcaller_5_20.vcf)
do 
awk '!/#/ {split ($10,c,":"); print c[2]}' ${i} | \
awk -F',' '{if (NF==4) print
else if (NF==3) print $0",0"; else if (NF==2) print $0",0,0" }' > ${i}.tmp
done

#sum pre and postcapture
for sample in 102 103 10 104 106 18 37 38 45 56 57 58 81 82 86 91 9
do paste -d',' ${sample}_2*_htcaller_5_20.vcf.tmp ${sample}_3*_htcaller_5_20.vcf.tmp | \
awk -F',' '{print $1+$5","$2+$6","$3+$7","$4+$8}' > ${sample}_merged_htcaller_5_20.vcf.tmp
done

for sample in 102 103 10 104 106 18 37 38 45 56 57 58 81 82 86 91 9; do mv ${sample}_2*_htcaller_5_20.vcf.tmp alone/; done
for sample in 102 103 10 104 106 18 37 38 45 56 57 58 81 82 86 91 9; do mv ${sample}_3*_htcaller_5_20.vcf.tmp alone/; done
ls *.tmp | wc -l

###22 unique samples
awk '!/#/{print $1"\t"$2}' 9_27432_AACGACGT-GGAGAACG_htcaller_5_20.vcf  | grep -Fwf - preandpostcapture.vcf | cut -f-8 > table.tmp2
paste -d'\t' *.tmp > table.tmp
paste -d'\t' table.tmp2 table.tmp > table.tmp3
ls *vcf.tmp | awk -F'_' '{print $1}' | awk 'BEGIN {ORS ="\t"} {print $0}' | sed -e '$a\' | paste -d'\t' <(echo -e "CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO") - > header
cat header table.tmp3 > table.tmp4
awk '{print NF}' table.tmp4 | sort | uniq #30 
awk 'length($4)!=1 || length($5)!=1' table.tmp4 | wc -l
### 20 snps are DEL, INS or have a second ALT.


samples=$(ls *raw.snps.indels.vcf | sed 's/^/--variant /g')
java -jar /home/icvv/nmauri/tools/GenomeAnalysisTK.jar \
-T GenotypeGVCFs --includeNonVariantSites \
-R /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
-o preandpostcapture.vcf \
$(echo $samples)
grep -v '^##' preandpostcapture.vcf > preandpostcapture.csv

COL_NUMBER=$(grep -v '#' preandpostcapture.vcf | head -1 | awk '{print NF}')

for col in $(seq 10 $COL_NUMBER)
do awk -v var=$col '!/#/{split ($var,c,":"); print c[2]}' preandpostcapture.vcf | \
awk -F',' '{if (NF==1) print $0",0,0,0"
else if (NF==2) print $0",0,0"
else if (NF==3) print $0",0"
else if (NF==4) print $0
else print ERROR}' > ${col}_preandpostcapture.col
done
grep ERROR *.col


paste <(ls *.col) <(head -1 preandpostcapture.csv | tr -s '\t' '\n' | tail -n +10) | while read n k; do mv $n $k.col; done
paste -d'\t' *.col > preandpostcapture.txt

for sample in 102 103 10 104 106 18 37 38 45 56 57 58 81 82 86 91 9; do paste -d',' ${sample}_2*.col ${sample}_3*.col |\
awk -F',' '{print $1+$5","$2+$6","$3+$7","$4+$8}' > ${sample}_merged.col; done

for sample in 102 103 10 104 106 18 37 38 45 56 57 58 81 82 86 91 9; do mv ${sample}_2*.col alone/; done
for sample in 102 103 10 104 106 18 37 38 45 56 57 58 81 82 86 91 9; do mv ${sample}_3*.col alone/; done
cat <(ls *.col | awk -F'_' '{print $1}' | tr -s '\n' '\t' | sed 's/$/\n/') <(paste -d'\t' *.col) | \
paste -d'\t' <(cut -f-9 preandpostcapture.csv) -  > preandpostcapture2.csv
awk 'length($4)!=1 || length($5)!=1' preandpostcapture2.csv | wc -l #20snps are DEL, INS or have a second ALT.
echo $COL_NUMBER #31

for col in $(seq 10 $COL_NUMBER); do awk -v var=${col} 'NR>1{split($var,b,",")
if(b[3]>0||b[4]>0) print "--" #FILTER_CONTAMINATION
else if(b[1]+b[2]==0) print "--" #FILTER_ND
else if(b[1]+b[2]<3) print "--" #FILTER_DP
else if(b[1]+b[2]>=3 && (b[1]+0.0001)/(b[1]+b[2])>0.90) print $4$4
else if(b[1]+b[2]>2 && (b[2]+0.0001)/(b[1]+b[2])>0.90) print $5$5

else print "--"}' preandpostcapture2.csv > ${col}_preandpostcapture2.col
done
cat <(ls *preandpostcapture2.col | \
awk -F'_' '{print $1}' | tr -s '\n' '\t' | sed 's/$/\n/') <(paste -d'\t' *preandpostcapture2.col) | \
paste -d'\t' <(cut -f-9 preandpostcapture.csv) - > preandpostcapture4.csv
#number of EXIT in genotyping per sample
for col in $(seq 10 $COL_NUMBER); do awk -v var=${col} 'NR>1 && $var!="--"{print $var}' preandpostcapture4.csv | wc -l ; done
#number of occurrences pr position
cut -f10- preandpostcapture4.csv | awk '{count=0;for(i=2; i<=NF; i++) {if($i!="--") { count++ }}; print count}'

paste -d'\t' *.tmp5 | paste table.tmp2 - | cat header - > table.tmp5


#at this point calling is done filteirng reads: only PE, min mappping quality20, min base quality 5, not soft-clipped bases, mot multimapping, not secondary alignmennt.
#so, only unique PE alignment with mapq>=20 baseq>=5

#[icvv@hidalgo 2.bam_files]$ for i in 56_27440_AGGTCCTT-CGCTGCTG 57_27441_AATCCAGT-GGAGAACG 58_27442_TACGCATC-ATCGCGAG 86_27446_ACCGATTC-CTTGCCGT 91_27447_AGATGAGT-GCGCC
#AGT; do awk '$1=="chr2" && $2==17198115' ${i}_raw.snps.indels.vcf; done
#chr2	17198115	.	C	<NON_REF>	.	.	.	GT:AD:DP:GQ:PL	0/0:4,0:4:6:0,6,90
#chr2	17198115	.	C	T,<NON_REF>	0	.	BaseQRankSum=0.600;ClippingRankSum=0.818;DP=119;MLEAC=0,0;MLEAF=0.00,0.00;MQ=42.09;MQ0=0;MQRankSum=2.237;ReadPosRankSum=2.237	GT:AD:DP:GQ:PL:SB	0/0:21,2,0:23:19:0,19,470,63,476,519:10,11,0,0
#chr2	17198115	.	C	<NON_REF>	.	.	.	GT:AD:DP:GQ:PL	0/0:6,0:6:17:0,18,141
#chr2	17198115	.	C	<NON_REF>	.	.	.	GT:AD:DP:GQ:PL	0/0:0,0:0:0:0,0,0
#this is the same we saw in igv, unless counting single alignment. 

#9.all positions and samples recompilation 

samples=$(ls *raw.snps.indels.vcf | sed 's/^/--variant /g')
java -jar /home/icvv/nmauri/tools/GenomeAnalysisTK.jar \
-T GenotypeGVCFs --includeNonVariantSites \
-R /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa \
-o my_cohort.vcf \
$(echo $samples)

#alter9
precapture=$(for sample in 102 103 10 104 106 18 37 38 45 56 57 58 81 82 86 91 9; do ls ${sample}_2*_raw.snps.indels.vcf; done | sed 's/^/--variant /g')
java -jar /home/icvv/nmauri/tools/GenomeAnalysisTK.jar -T GenotypeGVCFs --includeNonVariantSites \
-L 12000snps.bed -R /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa -o precapture.vcf $(echo $precapture)

postcapture=$(for sample in 102 103 10 104 106 18 37 38 45 56 57 58 81 82 86 91 9; do ls ${sample}_3*_raw.snps.indels.vcf; done | sed 's/^/--variant /g')
java -jar /home/icvv/nmauri/tools/GenomeAnalysisTK.jar -T GenotypeGVCFs --includeNonVariantSites -L 12000snps.bed \
-R /home/icvv/nmauri/vitis_info/genome_v1/Vitis_vinifera_nc_ct_mt.fa -o precapture.vcf $(echo $postcapture)

#there is some error in the 38_3 bam file 

#10.genotype_me

#!/bin/bash
# sysinfo_page - A script to genotype different positions of a cohort of samples.

VCF=$1
#VCF is a .vcf file recompliling the calling info of several samples of a cohort, product of the step 9 of GATK_like_calling_adna.sh script. 
LIST=$2
#LIST is a .tsv with at least wo columns: CHROM/ POS. 
MINDP=$3
#MINDP is the min number of alignments per sample over an specific positions. 

echo 'USAGE: ./genotype_me.sh\  <VCF> <LIST> <MINDP>'
echo "I want to genotype"
wc -l $LIST |awk '{print $1}' 
echo "positions" 
echo "in my cohort of samples:" 
grep 'CHROM' $VCF | cut -f10- 
echo "with a min total depth of" $MINDP.  

#10.filtering known positions
grep -Fwf <(awk '{print $1"\t"$2}' $LIST) $VCF > tmp1

#11.genotype positions
for col in 10 11 12 13 14
do
awk -v mindp=$MINDP -v col=${col} 'length($4)==1 && length($5)==1 && $5!~/,/{split ($col, sample,":|,")
if (sample[1]=="./.") geno="NA:NA:NA" #no call
else if (sample[1]!="./." && $5!="." && sample[3]==0 && sample[2]>=mindp) geno="REF:"$4$4":"sample[2] 
else if (sample[1]!="./." && $5!="." && sample[2]==0 && sample[3]>=mindp) geno="HOM:"$5$5":"sample[3]
else if (sample[1]!="./." && $5!="." && sample[2]!=0 && sample[3]!=0 && sample[2]+sample[3]>=mindp) geno="HET:"$4$5":"sample[2]+sample[3]
else if (sample[1]!="./." && $5=="." && sample[2]>=mindp) geno="REF:"$4$4":"sample[2] #no variant
else geno="NA:NA:NA" 
print geno}' tmp1 > ${col}.tmp2 #two columns
cut -d':' -f1 ${col}.tmp2 | sort | uniq -c | awk '{print $2"\t"$1}' > ${col}.tmp3
done

awk -v mindp=$MINDP -v list=$LIST 'length($4)==1 && length($5)==1 && $5!~/,/{print $1"\t"$2"\t"list"\t"$4"\t"$5"\t"$6"\tBQmin5_MQmin20_DPmin"mindp"\t"$8"\t3geno:2N:DP"}' tmp1 > tmp4
paste -d'\t' 1*.tmp2 > tmp5
echo -e "CHROM\tPOS\tLIST\tREF\tALT\tQUAL\tFILTER\tINFO\tCODE\t56\t57\t58\t86\t91" > header
cat header <(paste -d'\t' tmp4 tmp5) > ${MINDP}_${LIST}_${VCF}.csv
paste -d'\t' 1*.tmp3 > ${MINDP}_${LIST}_${VCF}.uniq

rm *tmp*
rm header

# merge both counting: non_enrichment and enrichment.

for col in 10 11 12 13 14
do 
awk -v col=${col} '{split ($col, sample,",")
if (sample[1]==0 && sample[2]==0) geno="--"
else if (sample[1]>=3 && sample[2]==0) geno=$4$4
else if (sample[1]==0 && sample[2]>=3) geno=$5$5
else if (sample[1]+sample[2]>=6 && sample[1]/(sample[1]+sample[2])>=0.2
sample[1]/(sample[1]+sample[2])<=0.8) geno=$4$5; else geno="NA"; print geno}' tmp2> ${col}.tmp2; done 
paste *.tmp2 
rm *.tmp2

#### martes
for col in $(seq 10 $columns)
do 
awk -v  col=${col} 'length($4)==1 && length($5)==1 && $5!~/,/{split ($col, sample,":|,"); if (sample[1]=="./.") geno="--" 
else if (sample[1]!="./." && $5=="." && sample[2]>=4) geno=$4$4 
else if (sample[1]!="./." && $5!="." && sample[3]==0 && sample[2]>=4) geno=$4$4
else if (sample[1]!="./." && $5!="." && sample[2]==0 && sample[3]>=4) geno=$5$5
else if (sample[1]!="./." && $5!="." && sample[2]!=0 && sample[3]!=0 && sample[2]+sample[3]>=4) geno=$4$5
else geno="error"
$col=geno; print $col}' tmp1 > ${col}_geno.tmp
done 

paste -d'\t' *_geno.tmp > geno.tmp
paste -d '\t' <(cut -f-10 tmp1) geno.tmp > geno.tmp2
cat header geno.tmp2 > geno.csv
