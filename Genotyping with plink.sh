plink2 --vcf 10.genotyping/adna_cohort_1.vcf --snps-only --allow-extra-chr --recode A-transpose --threads 8 --out 10.genotyping/adna_cohort_1_plink2
for i in adna_cohort_1_10207snps adna_cohort_1_14snps adna_cohort_1_25snps adna_cohort_1_48snps; do head -73 adna_cohort_1.vcf | cat - ${i} > ${i}.vcf; done
########################## gt data #########################
for i in adna_cohort_1_10207snps adna_cohort_1_14snps adna_cohort_1_25snps adna_cohort_1_48snps; do plink2 --vcf ${i}.vcf --snps-only --allow-extra-chr --recode A-transpose --threads 8 --out ${i}_plink2; done
########################## dp data #########################
for i in adna_cohort_1_48snps adna_cohort_1_14snps adna_cohort_1_25snps adna_cohort_1_10207snps; do vcftools --geno-depth --vcf ${i}.vcf --out ${i}; done
##remove when NA in the 21 samples
##########################better representated #############
for i in 14snps 48snps 25snps 10207snps; do tail -n +2 adna_cohort_1_${i}_plink2.traw | cut -f7- | awk -F'NA' '{print NF-1}' | awk '$1<21' | wc -l && echo ${i}; done  
13
14snps
41
48snps
23
25snps
9834
10207snps

##this is the number of snps at least in 1 sample.

for i in 14snps 48snps 25snps 10207snps; do tail -n +2 adna_cohort_1_${i}_plink2.traw | cut -f7- | awk '{for (i=1;i<=NF;i++) sum[i]+=$i;};END{for (i in sum) print sum[i]}' | awk '$1!="0"' | wc -l && echo ${i}; done
16
14snps
18
48snps
17
25snps
21
10207snps

# this is the number of samples (21) that have at least one of those snps. 

depends on the number of reads covering the position this is going to be for realible.


mkdir query database
cd database
tail -n +41 783_Vvinifera_10207SNP_genotyping_matrix_12X_v2_urgi_edited.vcf | \
sed 's/0\/0/2/g' | sed 's/0\/1/1/g' | sed 's/1\/1/0/g' | sed 's/1\/0/1/g' \ '0/1' and '1/0' are the same or het==1 (ref=G alt=A snp=AG). 
The conversion program (plink2 writes 1/0 is the order is different than ref alt but this is just an alphabetic order in this case.
Column 10 is the first id variety==B00174W


> 783_Vvinifera_10207SNP_genotyping_matrix_12X_v2_urgi_edited.table
#multiallelic positions 
#Exist 2 multiallelic snps in 14snps and 692 in the complete chip 10K
awk 'length($5)>1' 14snps.table | wc -l
2
awk 'length($5)>1' 10207snps.table | wc -l
692

cd 9.erc_bp
for i in $(cat ../samples.list); do \ 
plink --vcf ${i}_raw.snps.indels.g.vcf --double-id --snps-only --recode 23 --allow-extra-chr 0 --threads 8 --out ${i}; done
#the .txt file generated corresponds with 
10.genotyping/
empiezo con todas las posiciones y seleccionaré al final
################# dp data ###################################################
vcftools --geno-depth --vcf adna_cohort_1_total.vcf --out adna_cohort_1_total
VCFtools - 0.1.15
(C) Adam Auton and Anthony Marcketta 2009

Parameters as interpreted:
	--vcf adna_cohort_1_total.vcf
	--geno-depth
	--out adna_cohort_1_total

After filtering, kept 21 out of 21 Individuals
Outputting Depth for Each Genotype
After filtering, kept 487132837 out of a possible 487132837 Sites
Run Time = 4591.00 seconds
################# gt data in 012 as hom,het,ref #############################
plink2 --vcf adna_cohort_1_total.vcf --snps-only --allow-extra-chr --recode A-transpose --threads 8 --out adna_cohort_1_total
##################23andme is better to work with multiallelic then###########
# as plink conversion of .vcf to .23andme can only be done sample by sample:::: 
head -100 adna_cohort_1_total.vcf | grep '##' > header
cut -f-9 adna_cohort_1_total.vcf | grep -v '##' > rownames
#for i in $(seq 10 31) ; do cut -f${i} adna_cohort_1_total.vcf | grep -v '##' | paste -d'\t' rownames - | cat header -  > ${i}_adna_cohort_1_total.vcf; done
#at this point I have a .vcf per sample with the name of the column.
#only snps
for i in $(cat ../samples.list); do plink --vcf ${i}_raw.snps.indels.g.vcf --double-id --snps-only --recode 23 --allow-extra-chr 0 --threads 8 --out ${i}; done



